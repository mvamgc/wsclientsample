<?php
header('Content-type: application/json');

$con=mysqli_connect(getenv('IP'),"mvamgc","","c9");
if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
} else {
  $result = mysqli_query($con,"SELECT * FROM clients");
  echo '{"clients":[';
  $first=1;
  while($row = mysqli_fetch_array($result)) {
      if($first==1) {
          $first=0;
      } else {
          echo ',';
      }
    echo '{"id":"'.$row['id'] . '", "fullname":"'. $row['fullname'].'"}';
  }
  echo ']}';
}
mysqli_close($con);
?>

