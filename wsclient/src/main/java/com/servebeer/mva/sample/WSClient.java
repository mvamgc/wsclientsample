package com.servebeer.mva.sample;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URISyntaxException;

/**
 * Created by mva on 30/01/14.
 */
public class WSClient {
    private static final Logger LOG = LoggerFactory.getLogger(WSClient.class);

    private static final String URL = "https://wsclientsample-c9-mvamgc.c9.io/wsserver/clients.php";
    public String getAllClients() throws URISyntaxException, IOException {
        String result = null;
        InputStream is = null;
        CloseableHttpClient httpclient = null;
        try {
            HttpGet httpget = new HttpGet(URL);
            httpclient = HttpClients.createDefault();
            CloseableHttpResponse response = httpclient.execute(httpget);
            HttpEntity entity = response.getEntity();
            long len = entity.getContentLength();
            System.out.println(len);

            is = entity.getContent();
            result = readAsString(is);
            LOG.debug("result: {}", result);
        } finally {
            if(is != null) {
                is.close();
            }
            if(httpclient != null) {
                httpclient.close();
            }
        }
        return result;
    }


    private String readAsString(InputStream is) throws IOException {
        Reader rd = new InputStreamReader(is);
        StringBuilder str = new StringBuilder();
        char[] buf = new char[2048];

        for(;;) {
            int len = rd.read(buf);
            if(len <= 0) {
                break;
            } else {
                str.append(buf, 0, len);
            }
        }

        return str.toString();

    }
}